import java.util.Scanner;

public class VirtualPetApp {
    public static void main (String[] args) {
        Fox[] skulk = new Fox[4];

        for (int i = 0; i < skulk.length; i++) { 
            Scanner input = new Scanner(System.in);

            System.out.print("What is this fox's species?: ");
            String species = input.nextLine();

            System.out.print("What is their habitat?: ");
            String habitat = input.nextLine();

            System.out.print("Are they male or female?: ");
            String sex = input.nextLine();

            skulk[i] = new Fox(species, habitat, sex);

            if (i < skulk.length - 1)
                System.out.println("\nNext fox:");
            else 
                input.close();
        }
        
        System.out.println("\n" + "Your fourth fox: " + skulk[3].species + ", " + skulk[3].habitat + ", " + skulk[3].sex + "\n");

        skulk[0].Sound();
        skulk[0].whatEats();
        
    }
}
