public class Fox {
    public String species;
    public String habitat;
    public String sex;

    Fox(String species, String habitat, String sex) {
        this.species = species;
        this.habitat = habitat;
        this.sex = sex;
    }


    public void whatEats() {
        System.out.println("Foxes usually eat small mammals, insects, berries, birds carrion, marine invertebrates, sea birds and fish.");
    }

    public void Sound() {
        if (species.equals("arctic"))
            System.out.println("Barking yowl");
        else
            System.out.println("shriek/scream");
    }


    

}